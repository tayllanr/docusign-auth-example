# frozen_string_literal: true

module Docusign
  module Config
    # account informations
    CLIENT_ID = 'e4c37606-2522-46eb-9757-05967e347b75'
    USER_GUID = 'fe359286-2b99-4448-8db4-8b7d1b7ada73'
    SIGNER_EMAIL = 'tayllan@resolvvi.com'
    SIGNER_NAME = 'Messias Tayllan Teles Farias'
    AUTH_URL = 'account-d.docusign.com'

    # token settings
    TOKEN_REPLACEMENT_IN_SECONDS = 10 * 60 # 10 minutes
    TOKEN_EXPIRATION_IN_SECONDS = 60 * 60 # 1 hour
  end
end
