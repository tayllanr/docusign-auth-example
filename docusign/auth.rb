# frozen_string_literal: true

require_relative './config'

module Docusign
  # Fetch and Refresh Access Tokens to DOCUSIGN
  class Auth
    include Config

    attr_reader :account_id

    def initialize(client)
      @api_client = client
    end

    def token
      generate_token if @token.nil? || token_expired?

      @token
    end

    def generate_token
      rsa_private_key = File.expand_path('key.pem')

      @api_client.set_oauth_base_path(AUTH_URL)

      response = @api_client.request_jwt_user_token(CLIENT_ID, USER_GUID, rsa_private_key)

      @token = response.access_token
      @token_expiration_date = Time.now.to_f + TOKEN_EXPIRATION_IN_SECONDS

      set_account_settings if @account.nil?
    end

    def set_account_settings
      response = @api_client.get_user_info(@token)
      tmp_account = response.accounts.first

      @api_client.config.host = tmp_account.base_uri
      @account_id = tmp_account.account_id
    end

    private

    def token_expired?
      now = Time.now.to_i

      (now + TOKEN_REPLACEMENT_IN_SECONDS) > @token_expiration_date
    end
  end
end
