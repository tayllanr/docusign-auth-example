# frozen_string_literal: true

require './docusign/auth'
require 'docusign_esign'

config = DocuSign_eSign::Configuration.new
api_client = DocuSign_eSign::ApiClient.new(config)

dst = Docusign::Auth.new(api_client)
p dst.token
